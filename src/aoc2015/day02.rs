use tools::part::{Part, PartName};

const INPUT: &str = include_str!("day02.txt");

fn common_parser(s: &str) -> Vec<self::Box> {
    s.lines()
        .map(|l| {
            l.split('x')
                .map(|s| s.parse::<usize>().unwrap())
                .collect::<Vec<_>>()
        })
        .map(|a| self::Box {
            l: a[0],
            w: a[1],
            h: a[2],
        })
        .collect()
}

#[derive(Debug)]
struct Box {
    l: usize,
    w: usize,
    h: usize,
}

impl Box {
    fn paper(&self) -> usize {
        let areas = self.side_areas();
        let smallest_side = areas.iter().min().unwrap();
        self.surface_area() + smallest_side
    }

    fn surface_area(&self) -> usize {
        let sum: usize = self.side_areas().into_iter().sum();
        2 * sum
    }

    fn volume(&self) -> usize {
        self.l * self.w * self.h
    }

    fn side_areas(&self) -> [usize; 3] {
        [self.area_l_w(), self.area_w_h(), self.area_h_l()]
    }

    fn sides(&self) -> [usize; 3] {
        [self.l, self.w, self.h]
    }

    fn area_l_w(&self) -> usize {
        self.l * self.w
    }

    fn area_w_h(&self) -> usize {
        self.w * self.h
    }

    fn area_h_l(&self) -> usize {
        self.h * self.l
    }
}

#[derive(Debug)]
pub struct PartA {
    input: String,
    parsed: Vec<self::Box>,
}

impl Part for PartA {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 2;
    const PART: PartName = PartName::A;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
            parsed: vec![],
        }
    }

    fn parse(&mut self) {
        self.parsed = common_parser(self.input.as_ref());
    }

    fn calculate(&self) -> Self::Result {
        self.parsed.iter().map(|b| b.paper()).sum()
    }
}

impl_default_for!(PartA);

#[derive(Debug)]
pub struct PartB {
    input: String,
    parsed: Vec<self::Box>,
}

impl Part for PartB {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 2;
    const PART: PartName = PartName::B;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
            parsed: vec![],
        }
    }

    fn parse(&mut self) {
        self.parsed = common_parser(&self.input);
    }

    fn calculate(&self) -> Self::Result {
        let vs = self.parsed.iter().map(|b| b.volume());
        let s = self.parsed.iter().map(|b| {
            let mut sides = b.sides();
            sides.sort();
            let sum: usize = sides.iter().take(2).sum();
            2 * sum
        });

        vs.zip(s)
            .map(|(vol, sides): (usize, usize)| vol + sides)
            .sum()
    }
}

impl_default_for!(PartB);

gen_tests_for!(PartA => 1606483);
gen_tests_for!(PartB => 3842356);

gen_tests_for!(PartA;
    box_2_3_4_is_58: "2x3x4" => 58,
    box_1_1_10_is_43: "1x1x10" => 43
);

gen_tests_for!(PartB;
    box_2_3_4_is_34: "2x3x4" => 34,
    box_1_1_10_is_10: "1x1x10" => 14
);
