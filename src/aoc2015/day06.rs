use std::collections::{HashMap, HashSet};
use std::iter;
use tools::part::{*, PartName::*};
use tools::point::Point2D;

const INPUT: &str = include_str!("day06.txt");

#[derive(Debug)]
enum Command {
    TurnOn(Point2D<u16>, Point2D<u16>),
    TurnOff(Point2D<u16>, Point2D<u16>),
    Toggle(Point2D<u16>, Point2D<u16>),
}

fn parse_point(s: &str) -> Point2D<u16> {
    let r = s.split(',')
        .map(|n: &str| n.parse::<u16>().unwrap())
        .collect::<Vec<_>>();
    Point2D::new(r[0], r[1])
}

fn gen_sequence(p1: Point2D<u16>, p2: Point2D<u16>) -> impl Iterator<Item = Point2D<u16>> {
    (p1.x..(p2.x + 1))
        .flat_map(move |x| iter::repeat(x).zip(p1.y..(p2.y + 1)))
        .map(|(x, y)| Point2D::new(x, y))
}

#[derive(Debug)]
pub struct PartA {
    input: String,
    parsed: Vec<Command>,
}

impl Part for PartA {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 6;
    const PART: PartName = A;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
            parsed: vec![],
        }
    }

    fn parse(&mut self) {
        self.parsed = self.input
            .lines()
            .map(|s| s.split(' ').collect())
            .map(|a: Vec<&str>| match (a[0], a[1]) {
                ("turn", "on") => Command::TurnOn(parse_point(a[2]), parse_point(a[4])),
                ("turn", "off") => Command::TurnOff(parse_point(a[2]), parse_point(a[4])),
                ("toggle", _) => Command::Toggle(parse_point(a[1]), parse_point(a[3])), // Command::Toggle(parse_point(p1), parse_point(p2)),
                x => unimplemented!("{:?}", x),
            })
            .collect()
    }

    fn calculate(&self) -> Self::Result {
        let mut set: HashSet<Point2D<u16>> = HashSet::new();

        self.parsed.iter().for_each(|c| {
            use self::Command::*;
            match *c {
                TurnOn(p1, p2) => gen_sequence(p1, p2).for_each(|p| {
                    set.insert(p);
                    ()
                }),
                TurnOff(p1, p2) => gen_sequence(p1, p2).for_each(|p| {
                    set.remove(&p);
                    ()
                }),
                Toggle(p1, p2) => gen_sequence(p1, p2).for_each(|p| {
                    if set.contains(&p) {
                        set.remove(&p)
                    } else {
                        set.insert(p)
                    };
                    ()
                }),
            };
        });

        set.len()
    }
}

impl_default_for!(PartA);

#[derive(Debug)]
pub struct PartB {
    input: String,
    parsed: Vec<Command>,
}

impl Part for PartB {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 6;
    const PART: PartName = A;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
            parsed: vec![],
        }
    }

    fn parse(&mut self) {
        self.parsed = self.input
            .lines()
            .map(|s| s.split(' ').collect())
            .map(|a: Vec<&str>| match (a[0], a[1]) {
                ("turn", "on") => Command::TurnOn(parse_point(a[2]), parse_point(a[4])),
                ("turn", "off") => Command::TurnOff(parse_point(a[2]), parse_point(a[4])),
                ("toggle", _) => Command::Toggle(parse_point(a[1]), parse_point(a[3])), // Command::Toggle(parse_point(p1), parse_point(p2)),
                x => unimplemented!("{:?}", x),
            })
            .collect()
    }

    fn calculate(&self) -> Self::Result {
        let mut map: HashMap<Point2D<u16>, usize> = HashMap::new();

        self.parsed.iter().for_each(|c| {
            use self::Command::*;
            match *c {
                TurnOn(p1, p2) => gen_sequence(p1, p2).for_each(|p| {
                    *map.entry(p).or_insert(0) += 1;
                }),
                TurnOff(p1, p2) => gen_sequence(p1, p2).for_each(|p| {
                    let v = map.entry(p).or_insert(0);
                    *v = v.saturating_sub(1);
                }),
                Toggle(p1, p2) => gen_sequence(p1, p2).for_each(|p| {
                    *map.entry(p).or_insert(0) += 2;
                }),
            };
        });

        map.values().sum()
    }
}

impl_default_for!(PartB);

gen_tests_for!(PartA => 543903);
gen_tests_for!(PartB => 14687245);

gen_tests_for!(PartA;
    a: "turn on 0,0 through 999,999" => 1_000_000,
    b: "toggle 0,0 through 999,0" => 1_000,
    c: "turn on 0,0 through 999,999\ntoggle 0,0 through 999,0" => 999_000,
    d: "turn on 0,0 through 999,999\nturn off 499,499 through 500,500" => 999_996
);

gen_tests_for!(PartB;
    a: "turn on 0,0 through 0,0" => 1,
    b: "toggle 0,0 through 999,999" => 2_000_000
);
