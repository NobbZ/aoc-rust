use tools::part::{Part, PartName};

const INPUT: &str = include_str!("day05.txt");
const VOWELS: &[char; 5] = &['a', 'e', 'i', 'o', 'u'];

#[derive(Debug)]
struct WordsA(String);

impl WordsA {
    pub fn is_nice(&self) -> bool {
        let mut vowel = 0usize;
        let mut twice = false;
        let mut naughty_sub = false;

        let mut last: Option<char> = None;

        for c in self.0.chars() {
            if VOWELS.contains(&c) {
                vowel += 1;
            }

            twice = twice || last == Some(c);
            naughty_sub = naughty_sub || match (last, c) {
                (Some('a'), 'b') | (Some('c'), 'd') | (Some('p'), 'q') | (Some('x'), 'y') => true,
                _ => false,
            };

            last = Some(c);
        }

        vowel >= 3 && twice && !naughty_sub
    }
}

#[derive(Debug)]
struct WordB(String);

impl WordB {
    pub fn is_nice(&self) -> bool {
        let mut double_pair = false;
        let mut pair_with_gap = false;

        let mut prev: Option<char> = None;
        let mut prev_prev: Option<char> = None;

        for (idx, c) in self.0.chars().enumerate() {
            pair_with_gap = pair_with_gap || if let Some(ppc) = prev_prev {
                ppc == c
            } else {
                false
            };

            if let Some(pc) = prev {
                let pair = &[pc, c].iter().collect::<String>();
                let rem = &self.0[(idx + 1)..];
                double_pair = double_pair || rem.contains(pair);
            }

            if double_pair && pair_with_gap {
                return true;
            };

            prev_prev = prev;
            prev = Some(c);
        }

        false
    }
}

#[derive(Debug)]
pub struct PartA {
    input: String,
    parsed: Vec<WordsA>,
}

impl Part for PartA {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 5;
    const PART: PartName = PartName::A;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
            parsed: vec![],
        }
    }

    fn parse(&mut self) {
        self.parsed = self.input.lines().map(|s| WordsA(s.into())).collect()
    }

    fn calculate(&self) -> Self::Result {
        self.parsed.iter().filter(|w| w.is_nice()).count()
    }
}

impl_default_for!(PartA);

#[derive(Debug)]
pub struct PartB {
    input: String,
    parsed: Vec<WordB>,
}

impl Part for PartB {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 5;
    const PART: PartName = PartName::B;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
            parsed: vec![],
        }
    }

    fn parse(&mut self) {
        self.parsed = self.input.lines().map(|s| WordB(s.into())).collect()
    }

    fn calculate(&self) -> Self::Result {
        self.parsed.iter().filter(|w| w.is_nice()).count()
    }
}

impl_default_for!(PartB);

gen_tests_for!(PartA => 255);
gen_tests_for!(PartB => 55);

gen_tests_for!(PartA;
               ugknbfddgicrmopn_nice: "ugknbfddgicrmopn" => 1,
               aaa_nice: "aaa" => 1,
               jchzalrnumimnmhp_naughty: "jchzalrnumimnmhp" => 0,
               haegwjzuvuyypxyu_naughty: "haegwjzuvuyypxyu" => 0,
               dvszwmarrgswjxmb_naughty: "dvszwmarrgswjxmb" => 0,
               combined_examples: "ugknbfddgicrmopn\naaa\njchzalrnumimnmhp\nhaegwjzuvuyypxyu\ndvszwmarrgswjxmb\n" => 2
);

gen_tests_for!(PartB;
               qjhvhtzxzqqjkmpb_nice: "qjhvhtzxzqqjkmpb" => 1,
               xxyxx_nice: "xxyxx" => 1,
               uurcxstgmygtbstg_naughty: "uurcxstgmygtbstg" => 0,
               ieodomkazucvgmuy_naughty: "ieodomkazucvgmuy" => 0,
               combined: "qjhvhtzxzqqjkmpb\nxxyxx\nuurcxstgmygtbstg\nieodomkazucvgmuy" => 2
);
