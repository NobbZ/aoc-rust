use tools::part::{Part, PartName, PartName::*};

const INPUT: &str = "1321131112";

struct Grouped<T, I>
where
    T: PartialEq + Clone,
    I: Iterator<Item = T>,
{
    iter: I,
    last: Option<T>,
    end: bool,
}

fn grouped<T, I>(iter: I) -> Grouped<T, I>
where
    T: PartialEq + Clone,
    I: Iterator<Item = T>,
{
    Grouped {
        iter,
        last: None,
        end: false,
    }
}

impl<T, I> Iterator for Grouped<T, I>
where
    T: PartialEq + Clone,
    I: Iterator<Item = T>,
{
    type Item = Vec<T>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut result = vec![];

        if self.end {
            return None;
        }

        if self.last.is_none() {
            self.last = match self.iter.next() {
                Some(e) => Some(e),
                None => {
                    self.end = true;
                    return None;
                }
            }
        }

        loop {
            if let Some(last) = self.last.clone() {
                result.push(last);
            }

            match self.iter.next() {
                Some(e) => {
                    if Some(e.clone()) != self.last {
                        self.last = Some(e);
                        return Some(result);
                    }
                }
                None => {
                    self.end = true;
                    return Some(result);
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct PartA {
    input: String,
}

impl Part for PartA {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 10;
    const PART: PartName = A;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
        }
    }

    fn parse(&mut self) {}

    fn calculate(&self) -> Self::Result {
        let mut digits = self.input.clone();

        for _ in 0..40 {
            digits = grouped(digits.chars())
                .map(|g| format!("{}{}", g.len(), g[0]))
                .collect::<String>();
        }

        digits.len()
    }
}

#[derive(Debug)]
pub struct PartB {
    input: String,
}

impl Part for PartB {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 10;
    const PART: PartName = B;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
        }
    }

    fn parse(&mut self) {}

    fn calculate(&self) -> Self::Result {
        let mut digits = self.input.clone();

        for _ in 0..50 {
            digits = grouped(digits.chars())
                .map(|g| format!("{}{}", g.len(), g[0]))
                .collect::<String>();
        }

        digits.len()
    }
}

impl_default_for!(PartA);
impl_default_for!(PartB);

gen_tests_for!(PartA => 492982);
gen_tests_for!(PartB => 6989950);
