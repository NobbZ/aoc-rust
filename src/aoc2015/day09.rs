use permutohedron::Heap;
use std::collections::{HashMap, HashSet};
use std::usize;
use tools::part::{Part, PartName, PartName::*};

const INPUT: &str = include_str!("day09.txt");

#[derive(Debug)]
pub struct PartA {
    input: String,
    distances: HashMap<(String, String), usize>,
    locations: HashSet<String>,
}

impl Part for PartA {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 9;
    const PART: PartName = A;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
            distances: hashmap!{},
            locations: hashset!{},
        }
    }

    fn parse(&mut self) {
        let mut distances = HashMap::new();
        let mut locations = HashSet::new();

        self.input
            .lines()
            .map(|l| l.split(' ').collect::<Vec<_>>())
            .for_each(|dist_spec: Vec<&str>| {
                let a = dist_spec[0].to_owned();
                let b = dist_spec[2].to_owned();
                let dist: usize = dist_spec[4].parse().unwrap();

                distances.insert((a.clone(), b.clone()), dist);
                distances.insert((b.clone(), a.clone()), dist);
                locations.insert(a);
                locations.insert(b);
            });

        self.locations = locations;
        self.distances = distances;
    }

    fn calculate(&self) -> Self::Result {
        let mut locations: Vec<String> = self.locations.iter().cloned().collect();
        let perms = Heap::new(&mut locations);

        perms
            .map(|locs| {
                locs.iter()
                    .cloned()
                    .zip(locs[1..].iter().cloned())
                    .map(|(a, b)| self.distances[&(a.clone(), b.clone())])
                    .sum::<usize>()
            })
            .min()
            .unwrap()
    }
}

impl_default_for!(PartA);

#[derive(Debug)]
pub struct PartB {
    input: String,
    distances: HashMap<(String, String), usize>,
    locations: HashSet<String>,
}

impl Part for PartB {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 9;
    const PART: PartName = A;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
            distances: hashmap!{},
            locations: hashset!{},
        }
    }

    fn parse(&mut self) {
        let mut distances = HashMap::new();
        let mut locations = HashSet::new();

        self.input
            .lines()
            .map(|l| l.split(' ').collect::<Vec<_>>())
            .for_each(|dist_spec: Vec<&str>| {
                let a = dist_spec[0].to_owned();
                let b = dist_spec[2].to_owned();
                let dist: usize = dist_spec[4].parse().unwrap();

                distances.insert((a.clone(), b.clone()), dist);
                distances.insert((b.clone(), a.clone()), dist);
                locations.insert(a);
                locations.insert(b);
            });

        self.locations = locations;
        self.distances = distances;
    }

    fn calculate(&self) -> Self::Result {
        let mut locations: Vec<String> = self.locations.iter().cloned().collect();
        let perms = Heap::new(&mut locations);

        perms
            .map(|locs| {
                locs.iter()
                    .cloned()
                    .zip(locs[1..].iter().cloned())
                    .map(|(a, b)| self.distances[&(a.clone(), b.clone())])
                    .sum::<usize>()
            })
            .max()
            .unwrap()
    }
}

impl_default_for!(PartB);

gen_tests_for!(PartA => 207);
gen_tests_for!(PartB => 804);

gen_tests_for!(PartA;
    example: r#"London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141"# => 605
);

gen_tests_for!(PartB;
    example: r#"London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141"# => 982
);
