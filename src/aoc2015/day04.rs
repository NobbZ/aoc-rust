use tools::part::{Part, PartName::{self, *}};
use std::iter::repeat;
use crypto::md5::Md5;
use crypto::digest::Digest;

const INPUT: &str = "yzbqklnj";

fn gen_sequence(input: &str) -> impl Iterator<Item = (usize, [u8; 16])> {
    let i = input.to_owned();
    repeat(i).enumerate().map(|(n, s)| {
        let mut hasher = Md5::new();
        hasher.input(s.as_bytes());
        hasher.input(n.to_string().as_bytes());
        let mut out = [0u8; 16]; // 16 bytes for MD5
        hasher.result(&mut out);
        (n, out)
    })
}

#[derive(Debug)]
pub struct PartA {
    input: String,
}

impl Part for PartA {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 4;
    const PART: PartName = A;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
        }
    }

    fn parse(&mut self) {}

    fn calculate(&self) -> Self::Result {
        gen_sequence(&self.input)
            .filter(|&(_n, ref s)| 0 == u32::from(s[0]) + u32::from(s[1]) + u32::from(s[2] >> 4))
            .take(1)
            .collect::<Vec<_>>()[0]
            .0
    }
}

impl_default_for!(PartA);

#[derive(Debug)]
pub struct PartB {
    input: String,
}

impl Part for PartB {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 4;
    const PART: PartName = B;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
        }
    }

    fn parse(&mut self) {}

    fn calculate(&self) -> Self::Result {
        gen_sequence(&self.input)
            .filter(|&(_n, ref s)| 0 == u32::from(s[0]) + u32::from(s[1]) + u32::from(s[2]))
            .take(1)
            .collect::<Vec<_>>()[0]
            .0
    }
}

impl_default_for!(PartB);

gen_tests_for!(PartA => 282749);
gen_tests_for!(PartB => 9962624);

gen_tests_for!(PartA;
    abcdef: "abcdef" => 609043,
    pqrstuv: "pqrstuv" => 1048970
);
