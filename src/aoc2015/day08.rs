use std::borrow::Borrow;
use tools::part::{Part, PartName, PartName::*};

const INPUT: &str = include_str!("day08.txt");

fn len_a<S>(s: S) -> usize
where
    S: Borrow<str>,
{
    s.borrow()
        .chars()
        .fold((0, None, 0), |(cnt, last, hex), c| match (last, c, hex) {
            (None, '\\', 0) => (cnt, Some(c), 0),
            (None, '"', 0) => (cnt, None, 0),
            (None, _, 0) | (Some('\\'), '\\', 0) | (Some('\\'), '"', 0) => (1 + cnt, None, 0),
            (None, _, 1) => (cnt, None, 2),
            (None, _, 2) => (cnt + 1, None, 0),
            (Some('\\'), 'x', 0) => (cnt, None, 1),
            (_, _, _) => unreachable!("len_a: {:?} {:?} {:?}", last, c, hex),
        })
        .0
}

fn len_b(s: impl Borrow<str>) -> usize {
    s.borrow().chars().fold(0, |sum, c| match c {
        '"' | '\\' => 2 + sum,
        _ => 1 + sum,
    }) + 2 // for the added enclosing quotes
}

#[derive(Debug)]
pub struct PartA {
    input: String,
    parsed: Vec<String>,
}

impl Part for PartA {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 8;
    const PART: PartName = A;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
            parsed: vec![],
        }
    }

    fn parse(&mut self) {
        self.parsed = self.input.lines().map(|l| l.to_owned()).collect();
    }

    fn calculate(&self) -> Self::Result {
        self.parsed.iter().map(|s| s.len() - len_a(s.clone())).sum()
    }
}

#[derive(Debug)]
pub struct PartB {
    input: String,
    parsed: Vec<String>,
}

impl Part for PartB {
    type Result = usize;

    const YEAR: u16 = 2015;
    const DAY: u8 = 8;
    const PART: PartName = B;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
            parsed: vec![],
        }
    }

    fn parse(&mut self) {
        self.parsed = self.input.lines().map(|l| l.to_owned()).collect();
    }

    fn calculate(&self) -> Self::Result {
        self.parsed.iter().map(|s| len_b(s.clone()) - s.len()).sum()
    }
}

impl_default_for!(PartA);
impl_default_for!(PartB);

gen_tests_for!(PartA => 1342);
gen_tests_for!(PartB => 2074);

gen_tests_for!(PartA;
    a: r#""""# => 2,
    b: r#""abc""# => 2,
    c: r#""aaa\"aaa""# => 3,
    d: r#""\x27""# => 5
);

gen_tests_for!(PartB;
    a: r#""""# => 4,
    b: r#""abc""# => 4,
    c: r#""aaa\"aaa""# => 6,
    d: r#""\x27""# => 5
);
