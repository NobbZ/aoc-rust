use std::collections::HashMap;
use tools::part::{Part, PartName, PartName::*};

const INPUT: &str = include_str!("day07.txt");

mod parser {
    #![allow(unknown_lints)]
    #![allow(clippy)]
    include!(concat!(env!("OUT_DIR"), "/parse_2015_07.rs"));
}

#[derive(Debug, Clone)]
pub enum Input {
    Wire(String),
    Lit(u16),
}

impl From<String> for Input {
    fn from(s: String) -> Self {
        Input::Wire(s)
    }
}

impl From<u16> for Input {
    fn from(n: u16) -> Self {
        Input::Lit(n)
    }
}

#[derive(Debug, Clone)]
pub enum Instruction {
    Assign(Input, String),
    Not(Input, String),
    Or(Input, Input, String),
    And(Input, Input, String),
    RShift(Input, u16, String),
    LShift(Input, u16, String),
}

impl Instruction {
    fn get_wire(&self) -> String {
        use self::Instruction::*;
        match *self {
            Assign(_, ref w)
            | Not(_, ref w)
            | Or(_, _, ref w)
            | And(_, _, ref w)
            | RShift(_, _, ref w)
            | LShift(_, _, ref w) => w.clone(),
        }
    }

    fn val_for(&self, ctx: &mut HashMap<String, Instruction>) -> u16 {
        use self::Instruction::*;
        use self::Input::*;

        let val = match *self {
            Assign(Wire(ref w), _) => {
                let i = ctx.get(w).unwrap().clone();
                i.val_for(ctx)
            }
            Assign(Lit(n), _) => n,
            Not(Wire(ref w1), ref w) => {
                let i = ctx.get(w1).unwrap().clone();
                Not(Lit(i.val_for(ctx)), w.clone()).val_for(ctx)
            }
            Not(Lit(n), _) => !n,
            Or(Wire(ref w1), ref w2, ref w) => {
                let i = ctx.get(w1).unwrap().clone();
                Or(Lit(i.val_for(ctx)), w2.clone(), w.clone()).val_for(ctx)
            }
            Or(Lit(n), Wire(ref w2), ref w) => {
                let i = ctx.get(w2).unwrap().clone();
                Or(Lit(n), Lit(i.val_for(ctx)), w.clone()).val_for(ctx)
            }
            Or(Lit(n), Lit(m), _) => n | m,
            And(Wire(ref w1), ref w2, ref w) => {
                let i = ctx.get(w1).unwrap().clone();
                And(Lit(i.val_for(ctx)), w2.clone(), w.clone()).val_for(ctx)
            }
            And(Lit(n), Wire(ref w2), ref w) => {
                let i = ctx.get(w2).unwrap().clone();
                And(Lit(n), Lit(i.val_for(ctx)), w.clone()).val_for(ctx)
            }
            And(Lit(n), Lit(m), _) => n & m,
            RShift(Wire(ref w1), ref m, ref w) => {
                let i = ctx.get(w1).unwrap().clone();
                RShift(Lit(i.val_for(ctx)), *m, w.clone()).val_for(ctx)
            }
            RShift(Lit(n), m, _) => n >> m,
            LShift(Wire(ref w1), ref m, ref w) => {
                let i = ctx.get(w1).unwrap().clone();
                LShift(Lit(i.val_for(ctx)), *m, w.clone()).val_for(ctx)
            }
            LShift(Lit(n), m, _) => n << m,
        };

        ctx.insert(self.get_wire(), Assign(Lit(val), self.get_wire()));
        val
    }
}

#[derive(Debug)]
pub struct PartA {
    input: String,
    parsed: HashMap<String, Instruction>,
}

impl Part for PartA {
    type Result = u16;

    const YEAR: u16 = 2015;
    const DAY: u8 = 7;
    const PART: PartName = A;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
            parsed: hashmap![],
        }
    }

    fn parse(&mut self) {
        let ast: Vec<Instruction> = self::parser::instructions(&self.input).unwrap();

        ast.iter().for_each(|i| {
            self.parsed.insert(i.get_wire(), i.clone());
        });
    }

    fn calculate(&self) -> Self::Result {
        let mut ctx = self.parsed.clone();
        self.parsed["a"].val_for(&mut ctx)
    }
}

impl_default_for!(PartA);

#[derive(Debug)]
pub struct PartB {
    input: String,
    parsed: HashMap<String, Instruction>,
}

impl Part for PartB {
    type Result = u16;

    const YEAR: u16 = 2015;
    const DAY: u8 = 7;
    const PART: PartName = A;

    fn new(input: impl Into<String>) -> Self {
        Self {
            input: input.into(),
            parsed: hashmap![],
        }
    }

    fn parse(&mut self) {
        let ast: Vec<Instruction> = self::parser::instructions(&self.input).unwrap();

        ast.iter().for_each(|i| {
            self.parsed.insert(i.get_wire(), i.clone());
        });

        let b = {
            let mut p1: PartA = Default::default();
            p1.parse();
            p1.calculate()
        };

        self.parsed
            .insert("b".into(), Instruction::Assign(Input::Lit(b), "b".into()));
    }

    fn calculate(&self) -> Self::Result {
        let mut ctx = self.parsed.clone();
        self.parsed["a"].val_for(&mut ctx)
    }
}

impl_default_for!(PartB);

gen_tests_for!(PartA => 3176);
gen_tests_for!(PartB => 14710);
