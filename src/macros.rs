/// Generates testcases
#[macro_export]
macro_rules! gen_tests_for {
    (PartA => $exp:expr) => {
        gen_test!(test_full_part_a, PartA, Default::default(), $exp);
    };
    (PartB => $exp:expr) => {
        gen_test!(test_full_part_b, PartB, Default::default(), $exp);
    };
    (PartA; $( $n:ident: $input:expr => $exp:expr ),*) => {
        interpolate_idents! {$(
            gen_test!([test_sample_part_a_ $n], PartA, ::tools::part::Part::new($input), $exp);
        )*}
    };
    (PartB; $( $n:ident: $input:expr => $exp:expr ),*) => {
        interpolate_idents! {$(
            gen_test!([test_sample_part_b_ $n], PartB, ::tools::part::Part::new($input), $exp);
        )*}
    };
}

macro_rules! gen_test {
    ($name:ident, $t:ty, $gen:expr, $exp:expr) => {
        #[test]
        fn $name() {
            let mut part: $t = $gen;
            part.parse();
            assert_eq!(part.calculate(), $exp);
        }
    };
}

#[macro_export]
macro_rules! impl_default_for {
    ($ty:ty) => {
        impl Default for $ty {
            fn default() -> Self {
                Self::new(INPUT)
            }
        }
    };
}

#[macro_export]
macro_rules! inspect {
    ($e:expr) => {{
        let e = $e;
        println!("{:?}", e);
        e
    }};
}

#[macro_export]
macro_rules! mod_list {
    ($($id:ident ,)*) => {
        mod_list!($($id),*);
    };
    ($($id:ident),*) => {
        $(
            pub mod $id;
        )*
    }
}
