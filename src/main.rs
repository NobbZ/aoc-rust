#![feature(plugin)]
#![plugin(interpolate_idents)]
#![feature(unboxed_closures)]
#![feature(slice_patterns)]
#![feature(conservative_impl_trait)]
#![feature(universal_impl_trait)]
#![feature(collections)]

extern crate chrono;
extern crate crypto;
extern crate permutohedron;
extern crate progress;

#[macro_use]
extern crate maplit;

use progress::Progress;

use tools::part::Part;
use tools::stopwatch::StopWatch;

#[macro_use]
mod macros;
pub mod tools;

pub mod aoc2015;

macro_rules! replace_expr {
    ($_t:tt $sub:expr) => {$sub};
}

macro_rules! inner {
    ($t:ty) => {
        {
            let mut x: $t = Default::default();
            let ((), psw) = StopWatch::measure(&mut || x.parse());
            let (r, csw) = StopWatch::measure(&mut || x.calculate());
            println!(
                "\x1b[2K{year:04}-12-{day:02}{part}: {result:10} (parse: {psw}; calc: {csw}) <{url}>",
                year = <$t>::YEAR,
                day = <$t>::DAY,
                part = <$t>::PART,
                result = r,
                psw = psw,
                csw = csw,
                url = x.url(),
            );
        }
    };
}

macro_rules! aoc {
    ( $( $x:ty ),* ) => {
        {
            let mut progress = Progress::new("Progress", 0, <[()]>::len(&[$(replace_expr!($x ())),*]));
            progress.start();
            $({
                inner!($x);
                progress.increment();
            })*
        }
    };
}

fn main() {
    aoc!(
        aoc2015::day01::PartA,
        aoc2015::day01::PartB,
        aoc2015::day02::PartA,
        aoc2015::day02::PartB,
        aoc2015::day03::PartA,
        aoc2015::day03::PartB,
        aoc2015::day04::PartA,
        aoc2015::day04::PartB,
        aoc2015::day05::PartA,
        aoc2015::day05::PartB,
        aoc2015::day06::PartA,
        aoc2015::day06::PartB,
        aoc2015::day07::PartA,
        aoc2015::day07::PartB,
        aoc2015::day08::PartA,
        aoc2015::day08::PartB,
        aoc2015::day09::PartA,
        aoc2015::day09::PartB,
        aoc2015::day10::PartA,
        aoc2015::day10::PartB
    )
}
