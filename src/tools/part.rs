use std::fmt::{self, Debug, Display};
use std::default::Default;

#[derive(Debug)]
/// Identifies the parts of a days exercise.
pub enum PartName {
    /// The first part of the day
    A,
    /// The second part of the day
    B,
}

impl Display for PartName {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match *self {
                PartName::A => "a",
                PartName::B => "b",
            }
        )
    }
}

pub trait Part: Default + Debug {
    /// The resulting type of the calculation made by this part.
    type Result: Display;

    /// The year of the exercise.
    const YEAR: u16;
    /// The day of the exercise.
    const DAY: u8;
    /// The actual part of the exercise.
    const PART: PartName;

    /// Creates a new Part from scratch, providing the input as argument.
    fn new(input: impl Into<String>) -> Self;

    /// Parses the input that was given at creation time.
    fn parse(&mut self);
    /// Calculates the solution of the exercise.
    fn calculate(&self) -> Self::Result;

    /// Returns the URL of the exercise.
    fn url(&self) -> String {
        format!(
            "https://adventofcode.com/{year}/day/{day}",
            year = Self::YEAR,
            day = Self::DAY
        )
    }
}
