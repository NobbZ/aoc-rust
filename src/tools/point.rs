use std::ops::{Add, AddAssign};

/// Represents a point or coordinate on a 2D grid.
#[derive(Eq, PartialEq, Hash, Copy, Clone, Debug)]
pub struct Point2D<T> {
    pub x: T,
    pub y: T,
}

impl<T> Point2D<T> {
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T, U> Add<U> for Point2D<T>
where
    T: Add<T, Output = T> + Copy,
    U: Into<Point2D<T>>,
{
    type Output = Point2D<T>;

    fn add(self, rhs: U) -> Self::Output {
        let rhs = rhs.into();

        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl<'a, T, U> Add<U> for &'a Point2D<T>
where
    T: Add<T, Output = T> + Copy,
    U: Into<Point2D<T>>,
{
    type Output = Point2D<T>;

    fn add(self, rhs: U) -> Self::Output {
        *self + rhs
    }
}

impl<T, U> AddAssign<U> for Point2D<T>
where
    T: Add<T, Output = T> + Copy,
    U: Into<Point2D<T>>,
{
    fn add_assign(&mut self, rhs: U) {
        let rhs = rhs.into();

        *self = Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl<T, U, V> From<(U, V)> for Point2D<T>
where
    U: Into<T>,
    V: Into<T>,
{
    fn from(t: (U, V)) -> Self {
        let (x, y) = t;
        Self::new(x.into(), y.into())
    }
}
