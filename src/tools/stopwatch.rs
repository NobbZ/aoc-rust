use chrono::Duration;
use std::time::Instant;
use std::fmt::{self, Display};

/// Measures the time that passes between the first call to `start()` and the last call to `stop()`.
pub struct StopWatch {
    start: Option<Instant>,
    stop: Option<Instant>,
}

impl StopWatch {
    /// Starts the `StopWatch`
    pub fn start() -> Self {
        Self {
            start: Some(Instant::now()),
            stop: None,
        }
    }

    /// Measures the execution time of the passed in clojure and returns its result alongside a `StopWatch`.
    pub fn measure<R>(f: &mut FnMut() -> R) -> (R, Self) {
        let mut sw = Self::start();
        let r = f();
        sw.stop();
        (r, sw)
    }

    /// Stops the `StopWatch`.
    ///
    /// Former stop-times will get overwritten by this!
    pub fn stop(&mut self) {
        self.stop = Some(Instant::now());
    }

    /// Returns the time passed between starting and stopping as a `Duration`.
    ///
    /// If there was no call to `stop()` before, it will return `None` and `Some(duration)` otherwise.
    pub fn duration(&self) -> Option<Duration> {
        match *self {
            Self {
                start: Some(start),
                stop: Some(stop),
            } => Some(Duration::from_std(stop - start).unwrap()),
            _ => None,
        }
    }
}

impl Display for StopWatch {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match (self.start, self.stop) {
            (Some(start), Some(stop)) => {
                let dur = Duration::from_std(stop - start).unwrap();
                match (
                    dur.num_weeks(),
                    dur.num_days(),
                    dur.num_hours(),
                    dur.num_minutes(),
                    dur.num_seconds(),
                    dur.num_nanoseconds(),
                ) {
                    (0, 0, 0, 0, 0, Some(nano)) => write!(f, "{}nsec", nano),
                    (0, 0, 0, 0, sec, _) => {
                        write!(f, "{}.{:03}sec", sec, dur.num_milliseconds() % 1_000)
                    }
                    (0, 0, 0, min, sec, _) => write!(f, "{}:{:02}min", min, sec % 60),
                    (0, 0, h, min, _, _) => write!(f, "{}:{:02}h", h, min % 60),
                    (0, d, h, _, _, _) => write!(f, "{}d {}h", d, h % 24),
                    (w, d, _, _, _, _) => write!(f, "{}w {}d", w, d % 7),
                }
            }
            (Some(_), None) => write!(f, "Still running"),
            (None, None) => write!(f, "Not started"),
            _ => unreachable!("Not started but an end time, shouldn't happen…"),
        }
    }
}
