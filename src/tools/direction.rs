//! A module that contains types to represent various directions.

use tools::point::Point2D;

/// Represents arbitrary directions on a 2D grid.
#[derive(Debug, Copy, Clone)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

impl<T> Into<Point2D<T>> for Direction
where
    T: From<i32>,
{
    fn into(self) -> Point2D<T> {
        let (x, y) = match self {
            Direction::North => (1, 0),
            Direction::South => (-1, 0),
            Direction::East => (0, 1),
            Direction::West => (0, -1),
        };

        Point2D::new(x.into(), y.into())
    }
}
